// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef CUDAEXAMPLES_GPUMEMORYBLOB_H
#define CUDAEXAMPLES_GPUMEMORYBLOB_H

// Athena include(s).
#include "AthenaKernel/CLASS_DEF.h"

// System include(s).
#include <cstddef>

namespace AthCUDATutorial {

   /// Simple type that holds on to some amount of GPU memory
   ///
   /// The class does not provide any help with interpreting the GPU memory.
   /// It merely allows client code to access it, however they wish.
   ///
   class GPUMemoryBlob {

   public:
      /// Constructor with a memory pointer, and allocation size
      GPUMemoryBlob( void* ptr, std::size_t size = 0 );
      /// Destructor
      ~GPUMemoryBlob();

      /// Get the (non-const) pointer to the GPU memory
      void* ptr();
      /// Get the (const) pointer to the GPU memory
      const void* ptr() const;

      /// Get the size of the GPU memory
      std::size_t size() const;

   private:
      /// Pointer to the GPU memory
      void* m_ptr;
      /// Size of the GPU memory
      std::size_t m_size;

   }; // class GPUMemoryBlob

} // namespace AthCUDATutorial

// Declare a ClassID for the type
CLASS_DEF( AthCUDATutorial::GPUMemoryBlob, 3508626, 1 )

#endif // CUDAEXAMPLES_GPUMEMORYBLOB_H
