//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "../PreProcessorAlg.h"
#include "../ProcessorAlg.h"

// Declare the components to Gaudi.
DECLARE_COMPONENT( AthCUDATutorial::PreProcessorAlg )
DECLARE_COMPONENT( AthCUDATutorial::ProcessorAlg )
