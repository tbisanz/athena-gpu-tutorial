//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "caloClusterMaker.h"

// Athena include(s).
#include "AthCUDACore/StreamHolderHelpers.cuh"
#include "AthCUDACore/Macros.cuh"
#include "AthCUDACore/Memory.cuh"

// System include(s).
#include <cassert>

namespace AthCUDATutorial {

   /// Dummy kernel creating calo cluster energy values
   __global__
   void cuCaloClusterMaker( std::size_t arraySize, const int* digits,
                            float* energies ) {

      // Make sure that the kernel is executed in 1D.
      assert( blockIdx.y == 0 ); assert( threadIdx.y == 0 );
      assert( blockIdx.z == 0 ); assert( threadIdx.z == 0 );

      // Get the array index that this thread should operate on.
      const int index = blockIdx.x * blockDim.x + threadIdx.x;
      if( index >= arraySize ) {
         return;
      }

      // Calculate the "cluster energy" in some dummy way.
      energies[ index ] = 1.23f + 2.34f * digits[ index ];
   }

   void caloClusterMaker( const void* gpuMem, std::size_t gpuSize,
                          std::vector< float >& clusterE,
                          AthCUDA::StreamHolder& streamHolder ) {

      // Interpret the GPU memory blob in some simple way.
      const int* gpuInts = static_cast< const int* >( gpuMem );

      // Access the stream that we should be using.
      cudaStream_t stream = AthCUDA::getStream( streamHolder );
      assert( stream != nullptr );

      // Size of the GPU array.
      const std::size_t arraySize = gpuSize / sizeof( int );

      // Allocate some memory for the output of the cluster making.
      clusterE.resize( arraySize );
      auto devClusterE = AthCUDA::make_device_array< float >( arraySize );

      // Launch the kernel.
      static const int blockSize = 256;
      const int numBlocks = ( arraySize + blockSize - 1 ) / blockSize;
      static const int sharedSize = 0;
      cuCaloClusterMaker<<< numBlocks, blockSize, sharedSize, stream >>>(
         arraySize, gpuInts, devClusterE.get() );

      // Copy the output array back to the host.
      CUDA_EXP_CHECK( cudaMemcpyAsync( clusterE.data(), devClusterE.get(),
                                       arraySize * sizeof( float ),
                                       cudaMemcpyDeviceToHost, stream ) );

      // Check for errors, and wait for the operation to finish.
      CUDA_EXP_CHECK( cudaGetLastError() );
      CUDA_EXP_CHECK( cudaStreamSynchronize( stream ) );
   }

} // namespace AthCUDATutorial
