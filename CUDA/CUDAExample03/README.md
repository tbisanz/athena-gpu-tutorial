# Multiple Algorithms Cooperating

This example shows how one algorithm can pass data that's in GPU memory, to
another algorithm. The calculation performed by the code is nonsense, the point
is to demonstrate how an EDM object
([AthCUDATutorial::GPUMemoryBlob](CUDA/CUDAExample03/CUDAExample03/GPUMemoryBlob.h))
can be used to manage GPU memory with the help of
[StoreGateSvc](https://gitlab.cern.ch/atlas/athena/-/blob/master/Control/StoreGate/StoreGate/StoreGateSvc.h).

You can execute the example job with:

```
athena.py CUDAExample03/MultiAlgorithm_jobOptions.py
```

## Exercise(s)

  - Try to run the job with different numbers (1-4) of CPU threads, and check
    how the job behaves using
    [NSight Systems](https://developer.nvidia.com/nsight-systems).

```
nsys profile athena.py --threads=2 CUDAExample03/MultiAlgorithm_jobOptions.py
```

  - Update the toy
    [AthCUDATutorial::cuCaloClusterMaker(...)](CUDA/CUDAExample03/src/caloClusterMaker.cu)
    kernel to receive the value of a property set on the
    [AthCUDATutorial::ProcessorAlg](CUDA/CUDAExample03/src/ProcessorAlg.h)
    algorithm, and verify that the configuration argument properly affects what
    the GPU code would do.
