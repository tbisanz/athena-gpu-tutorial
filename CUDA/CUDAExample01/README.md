# Standalone Application in the Athena Environment

This example demonstrates how to build a simple executable (`cuda_helloworld`)
on top of an Athena environment.

The executable (out of the box) does not even interact with CUDA directly. It
merely calls on code from
[AthCUDACore](https://gitlab.cern.ch/atlas/athena/-/tree/master/Control/AthCUDA/AthCUDACore)
to print information about the available GPU(s).

## Exercise(s)

  - Retrieve how much shared memory the T4 GPU has available for one thread
    block. Use [cudaGetDeviceProperties(...)](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html)
    to do so.
    * Remember that in order to use the `<cuda_runtime_api.h>` header in the
      source file, you must use
      [FindCUDAToolkit](https://cmake.org/cmake/help/latest/module/FindCUDAToolkit.html)
      and `CUDA::cudart` in the package's configuration.
  - Include [GaudiKernel/StatusCode.h](https://gitlab.cern.ch/gaudi/Gaudi/-/blob/master/GaudiKernel/include/GaudiKernel/StatusCode.h)
    in the source file, and make sure that the build still succeeds. (By
    linking the executable to the `GaudiKernel` library.)
  - Rename [cuda_helloworld.cxx](CUDA/CUDAExample01/util/cuda_helloworld.cxx)
    to `cuda_helloworld.cu` (don't forget to update the CMake configuration!),
    and see how that affects your ability to make use of some of the
    Athena/Gaudi code directly.
