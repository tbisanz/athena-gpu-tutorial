# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

# Set up the job to read a test file from CVMFS.
import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections = [
   '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/ASG/DAOD_PHYS/v1/ttbar.FullSim.DAOD_PHYS.pool.root'
   ]

# Set up / access the algorithm sequence.
from AthenaCommon.AlgSequence import AlgSequence
algSequence = AlgSequence()

# Set up the service(s) needed by the job.
from AthenaCommon.AppMgr import ServiceMgr
streamPoolSvc = CfgMgr.AthCUDA__StreamPoolSvc( NStreams = 2 )
ServiceMgr += streamPoolSvc

# Add the algorithm to the sequence.
algSequence += CfgMgr.AthCUDATutorial__SingleAlgorithm( Blocking = True,
                                                 StreamPoolSvc = streamPoolSvc )

# Set up the avalanche scheduler correctly, in case we are running with multiple
# threads.
from AthenaCommon.ConcurrencyFlags import jobproperties
if jobproperties.ConcurrencyFlags.NumThreads() != 0:
   from AthenaCommon.AlgScheduler import AlgScheduler
   AlgScheduler.SchedulerSvc.PreemptiveBlockingTasks = True
   AlgScheduler.SchedulerSvc.MaxBlockingAlgosInFlight = 2
   pass

# Set the event printout interval.
if not hasattr( ServiceMgr, theApp.EventLoop ):
   ServiceMgr += getattr( CfgMgr, theApp.EventLoop )()
   pass
evtLoop = getattr( ServiceMgr, theApp.EventLoop )
evtLoop.EventPrintoutInterval = 10
