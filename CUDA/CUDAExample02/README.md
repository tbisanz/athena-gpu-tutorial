# Single Algorithm Using a GPU

This example shows how a single (reentrant) Athena algorithm can make use of
a GPU. The algorithm only communicates with other components on the host,
through host memory.

The algorithm simply allocates an array, and fills it with "random" numbers
based on the current event's `xAOD::EventInfo` information. Then it performs a
transformation on that array, storing the output of that transformation into
another array.

After compiling the project, you can execute a small job that uses the algorthm,
with:

```
athena.py CUDAExample02/SingleAlgorithm_jobOptions.py
```

## Exercise(s)

  - Try to run the job with `--threads=2`, and see what happens.
    * Modify how many "blocking algorithms" would be executed in parallel by the
      scheduler, to make the job succeed. You will need to edit
      [CUDAExample02/SingleAlgorithm_jobOptions.py](CUDA/CUDAExample02/share/SingleAlgorithm_jobOptions.py)
      for this.
  - Modify the code in [cudaFunction.cu](CUDA/CUDAExample02/src/cudaFunction.cu)
    to perform memory allocation and memory copying using "device memory" and
    not "managed memory".
    * Use [cudaMalloc(...)](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html)
      and [cudaMemcpy(...)](https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__MEMORY.html)
      to do this.
    * Check whether multi-threaded execution succeeds like that.
  - Profile the job using [NSight Systems](https://developer.nvidia.com/nsight-systems),
    and check whether using multiple streams in the job has any benefit for us.

```
nsys profile athena.py CUDAExample02/SingleAlgorithm_jobOptions.py
```

  - You can open the output file of this (`report1.qdrep`) either using
    `nsys-ui` on the `aiatlas-gpu-XX` node (after having logged into it with X11
    enabled), or using a local installation of NSight Systems.
  - Try to see if you can get calculations and host<->device memory copies to
    happen in parallel.
      * This requires using pinned host memory for the arrays, which would be
        allocated just once per thread. Note that this is **not** a trivial
        task...
