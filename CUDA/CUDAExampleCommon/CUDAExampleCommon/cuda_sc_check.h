// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//
#ifndef CUDAEXAMPLES_CUDA_SC_CHECK_H
#define CUDAEXAMPLES_CUDA_SC_CHECK_H

// CUDA include(s).
#include <cuda_runtime_api.h>

/// Helper macro for handling @c cudaError_t return types "Athena functions"
#define CUDA_SC_CHECK(EXP)                                                     \
   do {                                                                        \
      const cudaError_t ce = EXP;                                              \
      if( ce != cudaSuccess ) {                                                \
         ATH_MSG_ERROR( "Could not execute \"" #EXP "\" (reason: "             \
                        << cudaGetErrorString( ce ) << ")" );                  \
         return StatusCode::FAILURE;                                           \
      }                                                                        \
   } while( false )

#endif // CUDAEXAMPLES_CUDA_SC_CHECK_H
