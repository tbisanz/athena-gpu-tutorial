//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "SingleAlgorithm.h"
#include "hipFunction.h"

// Tutorial include(s).
#include "HIPExampleCommon/macros.h"

// Athena include(s).
#include "StoreGate/ReadHandle.h"

// HIP include(s).
#include <hip/hip_runtime_api.h>

// System include(s).
#include <vector>

namespace AthHIPTutorial {

   StatusCode SingleAlgorithm::initialize() {

      // Make sure that at least a single CUDA device is available.
      int devCount = 0;
      HIP_SC_CHECK( hipGetDeviceCount( &devCount ) );
      if( devCount == 0 ) {
         ATH_MSG_ERROR( "At least a single HIP device is necessary for this "
                        "algorithm!" );
         return StatusCode::FAILURE;
      }

      // Greet the user.
      ATH_MSG_INFO( "HIP device(s) available:" );
      for( int i = 0; i < devCount; ++i ) {
         hipDeviceProp_t props;
         HIP_SC_CHECK( hipGetDeviceProperties( &props, i ) );
         ATH_MSG_INFO( "  - name: " << props.name );
      }

      // Make sure that all devices are set to blocking synchronization.
      /*
      for( int i = 0; i < devCount; ++i ) {
         HIP_SC_CHECK( hipSetDevice( i ) );
         HIP_SC_CHECK( hipSetDeviceFlags( hipDeviceScheduleBlockingSync ) );
      }
      */

      // Initialise the algorithm's resources.
      ATH_CHECK( m_eiKey.initialize() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode SingleAlgorithm::execute( const EventContext& ctx ) const {

      // Retrieve the event info.
      SG::ReadHandle< xAOD::EventInfo > ei( m_eiKey, ctx );

      // Generate some numbers based on the event information.
      std::vector< float > a, b;
      static const std::size_t ARRAY_SIZE = 1000;
      a.reserve( ARRAY_SIZE ); b.resize( ARRAY_SIZE );
      for( std::size_t i = 0; i < ARRAY_SIZE; ++i ) {
         a.push_back( 1.23f + 1.2f * ( ei->eventNumber() +
                                       ei->runNumber() ) );
      }

      // Perform a parallel calculation.
      hipFunction( a, b );

      // Print some of the array elements for debugging.
      for( std::size_t i = 0; i < 10; ++i ) {
         ATH_MSG_DEBUG( "a[ " << i << " ] = " << a[ i ] << "; b[ " << i
                        << " ] = " << b[ i ] );
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthHIPTutorial
