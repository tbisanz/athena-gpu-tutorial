# Athena ROCm/HIP Examples

On one of the `aiatlas-gpu-XX` machines you can set up the build of the packages
in this directory with the following:

```sh
setupATLAS
source ./athena-gpu-tutorial/setup_hip_env.sh
mkdir build
cd build/
cmake ../athena-gpu-tutorial/
```

Note that this setup will use CUDA 11.2 installed on CVMFS, and ROCm/HIP 4.1
installed locally (under `/opt/rocm`) on the nodes to build the device code.

This code is only meant as extracurricular activity in the tutorial, mainly to
let you experiment with the HIP programming language on your own.
