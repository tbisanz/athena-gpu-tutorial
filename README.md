# Athena GPU Tutorial Code

This is the example code for the Athena GPU tutorial. It provides some simple
demonstrations for how one can use [CUDA](https://developer.nvidia.com/cuda-zone),
[ROCm/HIP](https://rocmdocs.amd.com) and [oneAPI/SYCL](https://www.oneapi.com/)
in an Athena environment.

The examples for the different languages are put into separate main directories.
Have a look at the README files in each of those packages for more information
about what they do.

Start by going to the [CUDA](CUDA/) directory, and go through the exercises one
by one. If time permits, or if you're particularly interested in those
languages, you can find some basic instructions for the [HIP](HIP/) and
[SYCL](SYCL/) examples as well.
