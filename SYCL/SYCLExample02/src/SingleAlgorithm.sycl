//
// Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "SingleAlgorithm.h"

// Athena include(s).
#include "StoreGate/ReadHandle.h"

// SYCL include(s).
#include <CL/sycl.hpp>

// System include(s).
#include <vector>

namespace AthSYCLTutorial {

   StatusCode SingleAlgorithm::initialize() {

      // List the available SYCL devices. (By definition we always have at least
      // the "host device" available to us.)
      ATH_MSG_INFO( "SYCL device(s) available:" );
      for( const cl::sycl::platform& platform :
           cl::sycl::platform::get_platforms() ) {
         for( const cl::sycl::device& device : platform.get_devices() ) {
            using devinf = cl::sycl::info::device;
            ATH_MSG_INFO( "  - name  : " << device.get_info< devinf::name >() );
            ATH_MSG_INFO( "  - vendor: "
                          << device.get_info< devinf::vendor >() );
         }
      }

      // Initialise the algorithm's resources.
      ATH_CHECK( m_eiKey.initialize() );

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

   StatusCode SingleAlgorithm::execute( const EventContext& ctx ) const {

      // Retrieve the event info.
      SG::ReadHandle< xAOD::EventInfo > ei( m_eiKey, ctx );

      // Generate some numbers based on the event information.
      std::vector< float > a, b;
      const std::size_t ARRAY_SIZE = 1000;
      a.reserve( ARRAY_SIZE ); b.resize( ARRAY_SIZE );
      for( std::size_t i = 0; i < ARRAY_SIZE; ++i ) {
         a.push_back( 1.23f + 1.2f * ( ei->eventNumber() +
                                       ei->runNumber() ) );
      }

      // Create the queue that the calculations will use.
      cl::sycl::queue queue;

      // Allocate memory on the device of the queue.
      float* devA =
         static_cast< float* >(
            cl::sycl::malloc_device( ARRAY_SIZE * sizeof( float ), queue ) );
      float* devB =
         static_cast< float* >(
            cl::sycl::malloc_device( ARRAY_SIZE * sizeof( float ), queue ) );
      if( ( devA == nullptr ) || ( devB == nullptr ) ) {
         ATH_MSG_ERROR( "Could not allocate device memory!" );
         return StatusCode::FAILURE;
      }

      // Copy the input data to the device.
      auto event = queue.memcpy( devA, a.data(), ARRAY_SIZE * sizeof( float ) );
      event.wait_and_throw();

      // Perform a calculation.
      queue.submit( [ ARRAY_SIZE, devA, devB ]( cl::sycl::handler& h ) {
         h.parallel_for< class LinearTransform >(
            cl::sycl::range< 1 >( ARRAY_SIZE ),
            [ ARRAY_SIZE, devA, devB ]( cl::sycl::id< 1 > id ) {

               // Exit early for invalid indices.
               if( id >= ARRAY_SIZE ) {
                  return;
               }

               // Perform some transformation.
               for( int i = 0; i < 1000; ++i ) {
                  devB[ id ] = cl::sycl::sin( devA[ id ] * 2.0f * i ) +
                               cl::sycl::cos( devA[ id ] * 2.43f * ( i - 10 ) );
               }
            } );
      } );
      queue.wait_and_throw();

      // Copy the output data back to the host.
      event = queue.memcpy( b.data(), devB, ARRAY_SIZE * sizeof( float ) );
      event.wait_and_throw();

      // Free the device memory.
      cl::sycl::free( devA, queue );
      cl::sycl::free( devB, queue );

      // Print some of the array elements for debugging.
      for( std::size_t i = 0; i < 10; ++i ) {
         ATH_MSG_DEBUG( "a[ " << i << " ] = " << a[ i ] << "; b[ " << i
                        << " ] = " << b[ i ] );
      }

      // Return gracefully.
      return StatusCode::SUCCESS;
   }

} // namespace AthSYCLTutorial
