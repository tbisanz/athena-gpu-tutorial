# Athena oneAPI/SYCL Examples

On one of the `aiatlas-gpu-XX` machines you can set up the build of the packages
in this directory with the following:

```sh
setupATLAS
source ./athena-gpu-tutorial/setup_sycl_env.sh
mkdir build
cd build/
cmake ../athena-gpu-tutorial/
```

Note that this setup will use CUDA 10.2 installed on CVMFS, and a hand-built
version of the Intel Clang compiler installed on AFS to build the device code.

This code is only meant as extracurricular activity in the tutorial, mainly to
let you experiment with the HIP programming language on your own.
