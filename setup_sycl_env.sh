# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Script setting up a build/runtime environment to use for the oneAPI/SYCL
# examples of the tutorial.
#

# Set up Athena-22.0.34.
asetup Athena,22.0.34

# Point CMake and Clang at the CVMFS installation of CUDA 10.2.
export CUDA_PATH=/cvmfs/sft.cern.ch/lcg/contrib/cuda/10.2/x86_64-centos7
source ${CUDA_PATH}/setup.sh

# Set up the Intel Clang compiler.
source /afs/cern.ch/user/k/krasznaa/work/public/clang/13.0.0-11ba5b57/x86_64-centos7-gcc8-opt/setup.sh
